import os, sys


class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout


comparisons = {"simplesearch": 0, "horspool": 0}    # zählt Anzahl Vergleiche in match()


with open("ABAB.txt") as file:
    abab_text = file.read()


with open("Sneewittchen_(1850).txt") as file:
    schneewitchen_text= file.read().lower().replace("ä","ae").replace("ö","oe").replace("ü","ue").replace("ß","ss").replace("„","\"").replace("“","\"")


def simple_search(p, s):
    m = len(p)
    n = len(s)
    hits = 0
    comparisons["simplesearch"] = 0
    for pos in range(n-m+1):
        if match(p, s[pos:pos+m], "simplesearch"):
            hits += 1
            print("Match an Position {0}".format(pos+1))
    return hits


def match(a, b, alg):
    m = len(a)
    for i in range(m):
    # for i in range(m - 1, -1, -1):
        comparisons[alg] += 1   # alg ist ein String mit Name des genutzten Algorithmus
        if a[i] != b[i]:
            return False
    return True


def horspool(p, s):
    hits = 0
    m = len(p)
    n = len(s)
    comparisons["horspool"] = 0
    shift = dict()
    for i in range(m-1):
        shift[p[i]] = m-i-1     # +1 da wir bei 0 beginnen; zb soll der Anfangsbuchstabe des Pattern nicht um m shiften, sondern m-1
    pos = 0
    while pos <= n-m:
        if match(p, s[pos:pos+m], "horspool"):
            print("Match an Position {0}".format(pos+1))
            hits += 1
        last_letter = s[pos+m-1]
        if last_letter in shift:
            pos += shift[last_letter]
        else:
            pos += m
    return hits


def horspool_sentence(p, s):
    m = len(p)
    n = len(s)
    comparisons["horspool"] = 0
    shift = dict()
    for i in range(m-1):
        shift[p[i]] = m-i-1     # +1 da wir bei 0 beginnen; zb soll der Anfangsbuchstabe des Pattern nicht um m shiften, sondern m-1
    pos = 0
    while pos < n-m:
        if match(p, s[pos:pos+m], "horspool"):
            print("Match an Position {0}".format(pos+1))
            print(find_sentence(pos, s))
        last_letter = s[pos+m-1]
        if last_letter in shift:
            pos += shift[last_letter]
        else:
            pos += m


def find_sentence(pos, s):
    start = 0
    for i in range(len(s)-pos):
        if s[pos+i] == "\"" or s[pos+i] == ".":
            end = pos+i+1
            break
    for i in range(pos):
        if s[pos - i] == "\"":
            start = pos-i
            break
        elif s[pos - i] == ".":
            start  = pos - i + 1
            break
    return s[start:end]


def main():
    # with open("Schneewittchen_lower.txt") as f:
    #     t = f.read()
    #
    # horspool("zwerg", t)
    # print(comparisons["horspool"])
    # horspool_sentence("zwerg", schneewitchen_text)
    with HiddenPrints():
        aba_hits = simple_search("ABA", abab_text)
        horspool("ABA", abab_text)
    print("ABA in ABAB Vergleiche: " + str(comparisons))
    print("ABA in ABAB Hits: " + str(aba_hits))
    with HiddenPrints():
        ccc_hits = simple_search("CCC", abab_text)
        horspool("CCC", abab_text)
    print("\nCCC in ABAB Vergleiche: " + str(comparisons))
    print("CCC in ABAB Hits: " + str(ccc_hits))
    print(len(abab_text))

    # print(schneewitchen_text)
    # horspool("aa","aaaaba")
    # print(comparisons["horspool"])
    p = "alloloa"
    s = "hallolalolahalloloallololalo"
    pattern1 = 'PONY'
    sequenz1 = 'PFERDPFERDPFERDPONYPFERDPONYPFERDPFERDPONY'
    pattern2 = 'PPPP'
    sequenz2 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
    # print(simple_search(pattern1, sequenz1))
    # print("Vergleiche simplesearch: {}\n".format(comparisons["simplesearch"]))
    # print(len(sequenz1))
    # print(simple_search(p,s))
    # print(comparisons["simplesearch"])
    # print(horspool(p,s))
    # print(comparisons["horspool"])
    # print(horspool(pattern2, sequenz2))
    # print("Vergleiche horspool: {}".format(comparisons["horspool"]))


main()
